# define the executable file 
MAIN = pomodoro

# Specify the file extension (i.e. 'c' or 'cpp')
FE = c

# define the C source files
# Multiple files (e.g. main.c symbol.c ..) or wildcard ($(wildcard *.c)) are also allowed
SRCS = $(wildcard *.$(FE))


#### Manual includes ####
# define any directories containing header files other than /usr/include
# Example: INCLUDES = -I/home/newhall/include  -I../include
INCLUDES =

# define library paths in addition to /usr/lib
# Example: LFLAGS = -L/home/newhall/lib  -L../lib
LFLAGS =

# define any libraries to link into executable:
#   The example will link in libmylib.so and libm.so:
# Example: LIBS = -lmylib -lm
LIBS =
#########################


# - define the C compiler to use
# - define the C object files 
ifeq ($(FE),c)
	CC = clang
	OBJS = $(SRCS:.c=.o)
else
	CC = clang++
	OBJS = $(SRCS:.cpp=.o)
endif


# define any compile-time flags
CFLAGS = -Wall -g

# The directory to store compiled (object '*.o') files
BUILD = .build


.PHONY: depend clean

all: $(MAIN)
	@echo The program has been compiled in the executable: $(MAIN)

$(MAIN):$(OBJS)
	$(CC) $(CFLAGS) $(INCLUDES) -o $(MAIN) $(BUILD)/$(OBJS) $(LFLAGS) $(LIBS)

# this is a suffix replacement rule for building .o's from .c's
# Plus, this generates compile_commands.json, useful for plugins like deoplete and ale
# (btw, see the gnu make manual section about automatic variables)
.$(FE).o: $(BUILD)
	mkdir -p $(BUILD)
	$(CC) -MJ $(BUILD)/$@.json $(CFLAGS) $(INCLUDES) -c $< -o $(BUILD)/$@
	sed -e '1s/^/[\n/' -e '$$s/,$$/\n]/' $(BUILD)/*.o.json > compile_commands.json

clean:
	$(RM) -r $(BUILD) *~ $(MAIN) compile_commands.json
