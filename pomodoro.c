#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <sys/types.h>
#include <pwd.h>

//#define abs(x) ((x)>0?(x):-(x))

// ANSII-colors:
//#define green_output printf("\x1b[32m")
//#define red_output printf("\x1b[31m")
//#define cyan_output printf("\x1b[36m")
//#define reset_color printf("\x1b[0m")

// hex-colors:
//#define green_output printf("^fg(#008000)")
#define green_output printf("^fg(#a6e22e)")
#define red_output printf("^fg(#ff0000)")
#define cyan_output printf("^fg(#00ffff)")
#define reset_color printf("^fg(#ffffff)")

int seconds_since_touched(char *filename)
{
  // Return the amount of seconds since the file (filename) changed
  // If longer than a year or a negative number (file not found) then return -1

  struct stat attr;
  time_t now, touched;
  int time_difference;

  now = time(NULL);
  stat(filename, &attr);
  touched = attr.st_ctime;
  time_difference = (int)difftime(now, touched);

  if(time_difference > 365*24*60*60 || time_difference < 0) return -1;
  return time_difference;
}

void progress_status(char *bar, int duration, int progress)
{
  int parts = (int) ((float) progress / ((float) duration / 5));
  sprintf(bar, "[%.*s%.*s]", parts, "####", 4-parts, "    ");
}

int pomodoro(char *session_file, int pomodoro_duration, int break_duration)
{
  int time_difference, countdown, minutes, seconds;
  char bar[6];

  // Run infinite loop
  for(;;)
  {
    time_difference = seconds_since_touched(session_file);
    if(time_difference < 0)
    {
      // Session file not found or older than a year
      printf("No pomodoro session");
    } else if(time_difference < 60 * pomodoro_duration) {
      // In pomodoro session! (green output)
      countdown = 60*pomodoro_duration - time_difference;
      minutes = countdown / 60;
      seconds = countdown % 60;
      progress_status(bar, pomodoro_duration, time_difference/60);
      green_output;
      printf("POMO. %s %.2i:%.2i", bar, minutes, seconds);
    } else if(time_difference < 60 * (pomodoro_duration + break_duration)) {
      // Break session! (orange output)
      time_difference = time_difference - (60 * pomodoro_duration);
      countdown = 60*break_duration - time_difference;
      minutes = countdown / 60;
      seconds = countdown % 60;
      progress_status(bar, break_duration, time_difference/60);
      cyan_output;
      printf("BREAK %s %.2i:%.2i", bar, minutes, seconds);
    } else {
      // Break is over! (red output)
      red_output;
      printf("Start new pomodoro!");
    }
    reset_color;
    printf("\n");
    fflush(stdout);
    sleep(1);
  }
  return 0;
}

int main()
{
  // For the pomodoro session file, use the home dir (2 lines below) plus "/.pomodoro_session"
  struct passwd *pw = getpwuid(getuid());
  char *session_file = pw->pw_dir;
  strcat(session_file, "/.pomodoro_session");

  // Set how long a pomodoro and how long a break should be (in minutes)
  int pomodoro_duration = 25;
  int break_duration = 5;

  return pomodoro(session_file, pomodoro_duration, break_duration);
}
